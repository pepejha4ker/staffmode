package ru.pepej.staff;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.pepej.staff.inventories.*;

import java.util.HashMap;
import java.util.Map;


public class Events implements Listener {
    StaffCore plugin;
    public static Map<Player, Player> pickedPlayer = new HashMap<>();

    public Events(StaffCore plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onOpen(InventoryOpenEvent e) {
        if (e.getInventory().equals(StartInventory.start)) {
            if (pickedPlayer.containsKey(e.getPlayer())) {
                pickedPlayer.remove(e.getPlayer());
            }
        }
    }

    @EventHandler
    public void onInventClick(InventoryClickEvent e) {
        if (!(e.getWhoClicked() instanceof Player)) return;
        Player p = (Player) e.getWhoClicked();
        if (e.getClickedInventory() != null) {
            if (e.getCurrentItem().getItemMeta() != null) {
                ItemStack clicked = e.getCurrentItem();
                ItemMeta meta = clicked.getItemMeta();
                if (e.getInventory().getTitle().equalsIgnoreCase(ChatColor.BLUE + "Игроки на сервере")) {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        if (player.getName().equalsIgnoreCase(meta.getDisplayName()) || ChatColor.stripColor(meta.getDisplayName()).contains(player.getName())) {
                            pickedPlayer.put(p, player);
                            ActionInventory.openAction(p);
                        }
                    }
                    e.setCancelled(true);
                } else if (e.getInventory().getTitle().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Выберите действие")) {
                    if (meta.getDisplayName().equalsIgnoreCase(ChatColor.RED + "" + ChatColor.BOLD + "Варн")) {
                        e.setCancelled(true);
                        WarnInventory.openWarning(p);
                    } else if (meta.getDisplayName().equalsIgnoreCase(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Телепорт")) {
                        e.setResult(Event.Result.DENY);
                        p.teleport(pickedPlayer.get(p));
                        p.sendMessage(API.format("&cВы были успешно телепортированы к игроку " + pickedPlayer.get(p).getName()));
                        pickedPlayer.remove(p);
                        p.closeInventory();
                    } else if (meta.getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "" + ChatColor.BOLD + "Вернуться назад")) {
                        e.setCancelled(true);
                        StartInventory.openStart(p);
                    } else if (meta.getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Просмотреть инвентарь")) {
                        e.setCancelled(true);
                        ShowInventory.openShow(p, pickedPlayer.get(p));
                    } else if (meta.getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Узнать Информацию об игроке")) {
                        e.setCancelled(true);
                        Inspect.inspectPlayer(p, pickedPlayer.get(p));
                    }
                    e.setCancelled(true);
                } else if (e.getInventory().getTitle().equalsIgnoreCase(ChatColor.DARK_AQUA + "Выберите причину")) {
                    if (meta.getDisplayName().equalsIgnoreCase(ChatColor.GRAY + "2.1")) {
                        API.sendDataMessage(p, "warn " + pickedPlayer.get(p).getName() + " 2.1");
                    }
                    if (meta.getDisplayName().equalsIgnoreCase(ChatColor.GRAY + "2.2")) {
                        API.sendDataMessage(p, "warn " + pickedPlayer.get(p).getName() + " 2.2");
                    }
                    if (meta.getDisplayName().equalsIgnoreCase(ChatColor.GRAY + "2.3")) {
                        API.sendDataMessage(p, "warn " + pickedPlayer.get(p).getName() + " 2.3");
                    }
                    if (meta.getDisplayName().equalsIgnoreCase(ChatColor.GRAY + "2.4")) {
                        API.sendDataMessage(p, "warn " + pickedPlayer.get(p).getName() + " 2.4");
                    }
                    if (meta.getDisplayName().equalsIgnoreCase(ChatColor.GRAY + "2.5")) {
                        API.sendDataMessage(p, "warn " + pickedPlayer.get(p).getName() + " 2.5");
                    }
                    if (meta.getDisplayName().equalsIgnoreCase(ChatColor.GRAY + "2.6")) {
                        API.sendDataMessage(p, "warn " + pickedPlayer.get(p).getName() + " 2.6");
                    }
                    if (meta.getDisplayName().equalsIgnoreCase(ChatColor.GRAY + "2.7")) {
                        API.sendDataMessage(p, "warn " + pickedPlayer.get(p).getName() + " 2.7");
                    }
                    if (meta.getDisplayName().equalsIgnoreCase(ChatColor.GRAY + "2.8")) {
                        API.sendDataMessage(p, "warn " + pickedPlayer.get(p).getName() + " 2.8");
                    }
                    if (meta.getDisplayName().equalsIgnoreCase(ChatColor.GRAY + "2.9")) {

                        API.sendDataMessage(p, "warn " + pickedPlayer.get(p).getName() + " 2.9");
                    }
                    if (meta.getDisplayName().equalsIgnoreCase(ChatColor.GRAY + "2.10")) {
                        API.sendDataMessage(p, "warn " + pickedPlayer.get(p).getName() + " 2.10");
                    }
                    if (meta.getDisplayName().equalsIgnoreCase(ChatColor.GRAY + "2.11")) {
                        API.sendDataMessage(p, "warn " + pickedPlayer.get(p).getName() + " 2.11");
                    }
                    if (clicked.getType().equals(Material.CHEST) && meta.getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "" + ChatColor.BOLD + "Вернуться назад")) {
                        ActionInventory.openAction(p);
                        e.setCancelled(true);
                    }
                    pickedPlayer.remove(p);
                    p.closeInventory();
                    e.setCancelled(true);
                } else if (e.getInventory().getTitle().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Просмотр инвентаря")) {
                    if (clicked.getType().equals(Material.CHEST) && meta.getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "" + ChatColor.BOLD + "Вернуться назад")) {
                        ActionInventory.openAction(p);
                        e.setCancelled(true);
                    }
                    e.setCancelled(true);
                } else if (e.getInventory().getTitle().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Информация об игроке")) {
                    if (clicked.getType().equals(Material.CHEST) && meta.getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "" + ChatColor.BOLD + "Вернуться назад")) {
                        ActionInventory.openAction(p);
                        e.setCancelled(true);
                    }
                    e.setCancelled(true);
                }
            }
        }
    }
}


