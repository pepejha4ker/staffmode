package ru.pepej.staff.inventories;
;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.*;
import org.bukkit.inventory.Inventory;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import ru.pepej.staff.StaffCore;


import java.util.ArrayList;
import java.util.List;



public class Inspect {
    public static Inventory inspect;

    static StaffCore plugin;

    public Inspect(StaffCore plugin) {
        this.plugin = plugin;
    }

    public static void inspectPlayer(Player p, Player t) {
        inspect = Bukkit.createInventory(null, 27, ChatColor.YELLOW + "" + ChatColor.BOLD + "Информация об игроке");
        int exp = t.getExpToLevel();
        int foodLvl = t.getFoodLevel();
        double hp = t.getHealth();
        String fly = t.getAllowFlight() ? ChatColor.AQUA + "В полёте" : ChatColor.DARK_GRAY + "Не летает";
        Location tLoc = t.getLocation();
        long x = Math.round(tLoc.getX());
        long y = Math.round(tLoc.getY());
        long z = Math.round(tLoc.getZ());
        long balalance = Math.round(StaffCore.getEconomy().getBalance(p));
        String world = tLoc.getWorld().getName();


        String gm = t.getGameMode().toString();
        switch (gm) {
            case "CREATIVE":
                gm = "Креатив";
                break;
            case "SURVIVAL":
                gm = "Выживание";
                break;
            case "ADVENTURE":
                gm = "Творческий";
                break;
            case "SPECTATOR":
                gm = "Наблюдатель";
                break;
        }
        PlayerInventory ti = t.getInventory();
        //
        //
        ItemStack health = new ItemStack(Material.APPLE, 1);
        ItemMeta hpm = health.getItemMeta();
        hpm.setDisplayName(ChatColor.RED + "Здоровье: " + ChatColor.GREEN + hp);
        health.setItemMeta(hpm);
        //
        //

        ItemStack confirm = new ItemStack(Material.STAINED_CLAY, 1, (short) 5);
        ItemMeta confirmm = confirm.getItemMeta();
        confirmm.setDisplayName(ChatColor.GREEN + "Подтвердить");
        confirm.setItemMeta(confirmm);
        //
        //
        ItemStack food = new ItemStack(Material.BREAD, 1);
        ItemMeta foodm = health.getItemMeta();
        foodm.setDisplayName(ChatColor.RED + "Уровень еды: " + ChatColor.GREEN + foodLvl);
        food.setItemMeta(foodm);
        //
        //
        ItemStack xp = new ItemStack(Material.EXP_BOTTLE, 1);
        ItemMeta xpm = xp.getItemMeta();
        xpm.setDisplayName(ChatColor.RED + "Уровень опыта: " + ChatColor.GREEN + exp);
        xp.setItemMeta(xpm);
        //
        //
        ItemStack gamem = new ItemStack(Material.GRASS, 1);
        ItemMeta gamemm = gamem.getItemMeta();
        gamemm.setDisplayName(ChatColor.RED + "Гейммод: " + ChatColor.GREEN + gm);
        gamem.setItemMeta(gamemm);
        //
        //
        ItemStack bal = new ItemStack(Material.REDSTONE, 1);
        ItemMeta balm = gamem.getItemMeta();
        balm.setDisplayName(ChatColor.RED + "Баланс: " + ChatColor.GREEN + balalance);
        bal.setItemMeta(balm);
        //
        //
        ItemStack flight = new ItemStack(Material.ELYTRA, 1);
        ItemMeta flightm = flight.getItemMeta();
        flightm.setDisplayName(ChatColor.RED + "Полёт: " + ChatColor.GREEN + fly);
        flight.setItemMeta(flightm);
        //
        ItemStack back = new ItemStack(Material.CHEST, 1);
        ItemMeta backm = back.getItemMeta();
        backm.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Вернуться назад");
        back.setItemMeta(backm);
        //
        //
        ItemStack coords = new ItemStack(Material.EMPTY_MAP, 1);
        ItemMeta coordsm = coords.getItemMeta();
        coordsm.setDisplayName(ChatColor.RED + "Координаты: ");
        List<String> lore = new ArrayList<>();
        lore.add(ChatColor.YELLOW + "X: " + ChatColor.GREEN + x);
        lore.add(ChatColor.YELLOW + "Y: " + ChatColor.GREEN + y);
        lore.add(ChatColor.YELLOW + "Z: " + ChatColor.GREEN + z);
        lore.add(ChatColor.YELLOW + "Мир: " + ChatColor.GREEN + world);
        coordsm.setLore(lore);
        coords.setItemMeta(coordsm);
        //
        ItemStack helmet = ti.getHelmet();
        ItemStack boots = ti.getBoots();
        ItemStack chestplate = ti.getChestplate();
        ItemStack leggins = ti.getLeggings();
        ItemStack mainHand = ti.getItemInMainHand();

        inspect.setItem(0, helmet);
        inspect.setItem(1, chestplate);
        inspect.setItem(2, leggins);
        inspect.setItem(3, boots);
        inspect.setItem(4, mainHand);
        inspect.setItem(9, gamem);
        inspect.setItem(10, bal);
        inspect.setItem(11, flight);
        inspect.setItem(13, coords);
        inspect.setItem(15, food);
        inspect.setItem(17, health);
        inspect.setItem(16, xp);
        inspect.setItem(26, back);
        p.openInventory(inspect);
    }
}
