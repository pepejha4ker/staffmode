package ru.pepej.staff.inventories;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.pepej.staff.StaffCore;

public class ActionInventory {

    public static Inventory action;

    static StaffCore plugin;

    public ActionInventory(StaffCore plugin) {
        this.plugin = plugin;
    }

    public static void openAction (Player p) {
        action = Bukkit.createInventory(null, 9, ChatColor.GOLD + "" + ChatColor.BOLD + "Выберите действие");
        ItemStack warn = new ItemStack(Material.ANVIL, 1);
        ItemMeta warnm = warn.getItemMeta();
        warnm.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "Варн");
        warn.setItemMeta(warnm);
        //
        ItemStack tp = new ItemStack(Material.ENDER_PEARL, 1);
        ItemMeta tpm = tp.getItemMeta();
        tpm.setDisplayName(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Телепорт");
        tp.setItemMeta(tpm);
        //
        ItemStack info = new ItemStack(Material.BOOK, 1);
        ItemMeta infom = info.getItemMeta();
        infom.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Узнать информацию об игроке");
        info.setItemMeta(infom);
        //
        ItemStack inv = new ItemStack(Material.BONE, 1);
        ItemMeta invm = inv.getItemMeta();
        invm.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Просмотреть инвентарь");
        inv.setItemMeta(invm);
        //
        ItemStack back = new ItemStack(Material.CHEST, 1);
        ItemMeta backm = back.getItemMeta();
        backm.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Вернуться назад");
        back.setItemMeta(backm);

        action.setItem(0, warn);
        action.setItem(2, tp);
        action.setItem(4, info);
        action.setItem(6, inv);
        action.setItem(8, back);
        p.openInventory(action);
    }

}
