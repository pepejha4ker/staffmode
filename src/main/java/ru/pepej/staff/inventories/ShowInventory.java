package ru.pepej.staff.inventories;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.pepej.staff.StaffCore;

public class ShowInventory {

    static StaffCore plugin;

    public static Inventory show;

    public ShowInventory(StaffCore plugin) {
        this.plugin = plugin;
    }

    public static void openShow(Player p, Player t) {
        show = Bukkit.createInventory(null, 45, ChatColor.YELLOW + "" + ChatColor.BOLD + "Просмотр инвентаря");

        ItemStack[] content = t.getInventory().getContents();

        ItemStack back = new ItemStack(Material.CHEST, 1);
        ItemMeta backm = back.getItemMeta();
        backm.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Вернуться назад");
        back.setItemMeta(backm);

        show.setContents(content);
        show.setItem(44, back);
        p.openInventory(show);

    }

}
