package ru.pepej.staff.inventories;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import org.bukkit.inventory.meta.SkullMeta;
import ru.pepej.staff.StaffCore;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class StartInventory {

    public static Inventory start;

    static StaffCore plugin;

    public StartInventory(StaffCore plugin) {
        this.plugin = plugin;
    }

    public static void openStart(Player p) {
        start = Bukkit.createInventory(null, 54, ChatColor.BLUE + "Игроки на сервере");
        Collection<? extends Player> onlinePlayers = Bukkit.getOnlinePlayers();
        for(Player online : onlinePlayers) {
            ItemStack skull = new ItemStack(Material.SKULL_ITEM ,1, (short) 3);
            SkullMeta meta = (SkullMeta) skull.getItemMeta();
            meta.setOwningPlayer(online);
            meta.setDisplayName(online.getName());
            skull.setDurability((short) SkullType.PLAYER.ordinal());
            skull.setItemMeta(meta);
            start.addItem(skull);
        }
        p.openInventory(start);
    }
}
