package ru.pepej.staff;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;

import org.bukkit.command.ConsoleCommandSender;

import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class StaffCore extends JavaPlugin {
    private static Economy econ = null;
    ConsoleCommandSender cs = Bukkit.getConsoleSender();
    API api;

    @Override
    public void onEnable() {
        if (!setupEconomy() ) {
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        api = new API(this);
        getServer().getMessenger().registerOutgoingPluginChannel(this, "StaffMode");
        cs.sendMessage("§8§m---------");
        cs.sendMessage(" §bStaffMode");
        cs.sendMessage(" §aEnabled!");
        cs.sendMessage("§8§m---------");
        //
        registerConfig();
        registerEvents();
        registerCommands();
    }

    private void registerConfig() {
        getConfig().options().copyDefaults(true);
        reloadConfig();
        saveConfig();
    }

    private void registerCommands() {
//        getCommand("staffmode").setExecutor(new StaffMode(this));
        getCommand("staffmenu").setExecutor(new StaffMode(this));
    }

    private void registerEvents() {
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new Events(this), this);
    }

    @Override
    public void onDisable() {
        for (Player online : getServer().getOnlinePlayers()) {
            if (Events.pickedPlayer.containsKey(online)) {
                online.closeInventory();
                Events.pickedPlayer.remove(online);
            }
//            if (StaffMode.staffmode.contains(online)) {
//                API.backInventory(online);
//                StaffMode.staffmode.remove(online);
//            }
//            if(API.fly.contains(online)) API.Fly(online);
//            if(API.vanished.contains(online)) API.Vanish(online);
//        }
        }
    }
    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    public static Economy getEconomy() {
        return econ;
    }
}
