package ru.pepej.staff;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class API {

    private static StaffCore plugin;

    public static ArrayList<Player> vanished = new ArrayList<>();
    public static ArrayList<Player> fly = new ArrayList<>();

    public API(StaffCore plugin) {
        this.plugin = plugin;
    }


    public static void sendDataMessage(Player player, String data) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF(data);
            out.writeUTF(player.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }

        player.sendPluginMessage(plugin, "StaffMode", b.toByteArray());
    }

//    public static void Vanish(Player p) {
//        if (!vanished.contains(p)) {
//            p.sendMessage(API.format(plugin.getConfig().getString("Messages.Vanish-Enabled")));
//            for (Player online : Bukkit.getOnlinePlayers()) {
//                if (online.hasPermission(plugin.getConfig().getString("Perms.Vanish-Bypass"))) {
//                    online.showPlayer(p);
//                } else {
//                    online.hidePlayer(p);
//                }
//                List<PotionEffect> effects = (List<PotionEffect>) p.getActivePotionEffects();
//                for (PotionEffect eff : effects) {
//                    StaffMode.pEff.put(p, effects);
//                    p.removePotionEffect(eff.getType());
//                }
//            }
//            vanished.add(p);
//        } else {
//            p.sendMessage(API.format(plugin.getConfig().getString("Messages.Vanish-Disabled")));
//            for (Player online : Bukkit.getOnlinePlayers()) {
//                online.showPlayer(p);
//            }
//            if(StaffMode.pEff.containsKey(p)) {
//                for (PotionEffect effect : StaffMode.pEff.get(p)) {
//                    p.addPotionEffect(effect);
//                }
//                StaffMode.pEff.remove(p);
//            }
//            vanished.remove(p);
//        }
//    }

    public static String format(String msg) {
        return ChatColor.translateAlternateColorCodes('&', msg);
    }

//    public static void Fly(Player p) {
//        if (fly.contains(p)) {
//            p.sendMessage(API.format(plugin.getConfig().getString("Messages.Fly-Disabled")));
//            p.setAllowFlight(false);
//            p.setFlying(false);
//            fly.remove(p);
//        } else {
//            p.sendMessage(API.format(plugin.getConfig().getString("Messages.Fly-Enabled")));
//            fly.add(p);
//            p.setAllowFlight(true);
//            p.setFlying(true);
//        }
//    }

}
