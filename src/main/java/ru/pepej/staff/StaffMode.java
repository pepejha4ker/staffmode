package ru.pepej.staff;


import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ru.pepej.staff.inventories.ActionInventory;
import ru.pepej.staff.inventories.StartInventory;

import java.awt.*;


public class StaffMode implements CommandExecutor {

    private static StaffCore plugin;

    public StaffMode(StaffCore plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equalsIgnoreCase("staffmenu")) {
            if (!(sender instanceof Player)) return true;
            Player p = (Player) sender;
            if (!p.hasPermission(plugin.getConfig().getString("Perms.Main-Command-Permission"))) {
                sender.sendMessage("§cНет прав!");
                return true;
            }
            for (Player p1 : Events.pickedPlayer.keySet()) {
                Events.pickedPlayer.remove(p1);
            }
            if (args.length == 1) {
                String targetName = args[0];
                Player target = Bukkit.getPlayer(targetName);
                if (target == null) {
                    sender.sendMessage("§cИгрок не найден!");
                    return true;
                }
                Events.pickedPlayer.put(p, target);
                ActionInventory.openAction(p);
                return true;
            } else if (args.length == 0) {
                StartInventory.openStart(p);
            }
        }
        return true;
    }
}
